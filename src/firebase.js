import fb from "firebase/app";
import "firebase/database";
import config from "./fbConfig";

let fbApp;

try {
  fbApp = fb.initializeApp(config);
} catch (e) {
  console.error(e);
}

export default fbApp.database();
