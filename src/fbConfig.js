const config = {
  apiKey: process.env.FIRE,
  authDomain: "pocketbot-40684.firebaseapp.com",
  databaseURL: "https://pocketbot-40684.firebaseio.com",
  projectId: "pocketbot-40684",
  storageBucket: "pocketbot-40684.appspot.com",
  messagingSenderId: "969731605928"
};

export default config;
