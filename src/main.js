import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import VueFire from "vuefire";
import VueWorker from "vue-worker";

Vue.config.productionTip = false;
Vue.use(VueFire);
Vue.use(VueWorker);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
