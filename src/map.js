const mapping = {
  /**
   * Converts an initial into the color name
   *
   * @param {String} c The color's initial
   */
  tColor(c) {
    switch (c) {
      case "R":
        return "red";
      case "B":
        return "blue";
      case "G":
        return "green";
      case "Y":
        return "yellow";
      case "C":
        return "chip";
      default:
        return c;
    }
  },
  fortify(x) {
    const { winner, wL, that } = x;
    const { cachedWorld, wlogPush, formatLog } = that;

    // Find a weak one
    const regDef1 = winner.filter(r => r.defense === 1);

    if (regDef1.length > 0) {
      let regToFortify = regDef1[0][".key"];

      // Check if we have a specific one rallied
      const rallyCheck = regDef1.map(r => r[".key"]);
      if (rallyCheck.includes(that.rallyPoints[wL.w]))
        regToFortify = that.rallyPoints[wL.w];

      that.debug(`♜ Fortifying the 1 star #${regToFortify}`);
      that.setRegion(regToFortify, 2);
      wlogPush.unshift(
        formatLog(wL, "stockpiled meat", cachedWorld[regToFortify])
      );
      return true;
    }

    // Try one more time with Def 2 regions
    const regDef2 = winner.filter(r => r.defense === 2);

    if (regDef2.length > 0) {
      let regToFortify = regDef2[0][".key"];

      // Check if we have a specific one rallied
      const rallyCheck = regDef2.map(r => r[".key"]);
      if (rallyCheck.includes(that.rallyPoints[wL.w]))
        regToFortify = that.rallyPoints[wL.w];

      that.debug(`♜ Fortifying the 2 star #${regToFortify}`);
      that.setRegion(regToFortify, 3);
      wlogPush.unshift(
        formatLog(wL, "stockpiled meat", cachedWorld[regToFortify])
      );
      return true;
    }

    that.debug(`♜ Fortifying nothing, I guess we're maxed`);
    that.sugarPoints[wL.w] += 50;
    wlogPush.unshift(formatLog(wL, "has stockpiled sugar", { ".key": false }));
    return true;
  },
  /**
   * The primary attacking function. Decides who to attack and where when a bar has been filled.
   * @param {Object} x - Just a payload. Contains...
   * @param {Object} x.wL - The match data
   * @param {Object} x.that - and everything else
   * @param {*} strength - How strong of an attack it will be
   */
  attack(x, strength = 1) {
    const { wL, that } = x;
    const { cachedWorld, wlogPush, formatLog, rallyPoints, debug } = that;
    const wColor = mapping.tColor(wL.w);
    const lColor = mapping.tColor(wL.l);
    let hineighbor = false;

    debug(`Faction bar filled, attacking: ${lColor}`);

    // First do checks against the rally point
    const wRally = rallyPoints[wL.w];
    if (wRally !== false) {
      // Get the teams of each neighbor of rally point
      const rallyNeighbors = cachedWorld[wRally].neighbors.map(
        n => cachedWorld[n].faction
      );

      const rallyOnLoser = cachedWorld[wRally].faction === lColor;
      const rallyOnChip = cachedWorld[wRally].faction === "chip";

      // Check if rally point is directly on losing enemy (or Chip)
      // And make sure we touch that region
      if ((rallyOnLoser || rallyOnChip) && rallyNeighbors.includes(wColor)) {
        debug(`Rally is set to direct attack #${wRally}`);
        hineighbor = wRally;
      }
    }

    // If no valid rally was set
    if (hineighbor === false) {
      const loser = that[`${mapping.tColor(wL.l)}Regions`];

      // Check each of loser's region and find 1 touching a winner region
      loser.forEach(reg => {
        reg.neighbors.forEach(n => {
          if (
            cachedWorld[n].faction === wColor &&
            cachedWorld[n].defense > 0 &&
            hineighbor === false
          )
            hineighbor = reg[".key"];
        });
      });

      if (hineighbor !== false)
        debug(`Ignoring rally, attacking nearest neighbor ${hineighbor}`);
    }

    // If we found a neighboring region to attack
    if (hineighbor !== false) {
      // Found region touching winner
      const def = cachedWorld[hineighbor].defense;
      const isChip = cachedWorld[hineighbor].faction === "chip";

      // If the original defense is overtaken by winner strength
      if (def - strength < 0) {
        that.setRegion(hineighbor, strength, wColor);
        wlogPush.unshift(
          formatLog(wL, "feasted on", cachedWorld[hineighbor], isChip)
        );

        // that.sugarReduction(wL.l);
        return true;
      }

      // Otherwise just lower it
      const defense = cachedWorld[hineighbor].defense - strength;

      if (defense === 0) {
        that.setRegion(hineighbor, 1, wColor);
        wlogPush.unshift(
          formatLog(wL, "feasted on", cachedWorld[hineighbor], isChip)
        );
        // that.sugarReduction(wL.l);
      } else {
        that.setRegion(hineighbor, defense);
        wlogPush.unshift(
          formatLog(wL, "weakened the", cachedWorld[hineighbor], isChip)
        );
      }

      debug("💥 Finalized attack.");
      return true;
    } else {
      // Chip can't fortify
      if (wL.w !== "C")
        mapping.fortify({
          winner: that[`${wColor}Regions`],
          wL,
          that
        });
    }

    return false;
  },
  /**
   * The primary function for bar calculations
   * @param {Object} data - The data from each match entry
   * @param {String} stamp - WL, W-, or -L, denotes match type
   * @param {Object} that - Everything else.
   */
  fillBars(data, stamp, that) {
    const { w: winner, l: loser, wRank, lRank, wID, lID, wStress } = data;
    const endgame = data.endgame || false;

    // Set match stress level based off lowest level between players
    let stress = 1;
    if (data.stress) {
      switch (data.stress) {
        case 2:
        case "med":
          stress = 0.5;
          break;
        case 1:
        case "low":
          stress = false;
          break;
        default:
          stress = 1; // High
      }
    }

    // Set ranks
    let wr = "M";
    let lr = "M";

    // Winners
    if (wRank <= 300) wr = "L";
    if (wRank >= 800 || wID === that.chip) wr = "H";

    // Losers
    if (lRank <= 300) lr = "L";
    if (lRank >= 800 || lID === that.chip) lr = "H";

    // W/R Tables
    const { wSugar, lSugar } = that;

    // Check if any faction bars exceeded 100
    let didAttack = false;
    let didReduce = false;
    let didTeam = false;
    let matchup;

    if (!that.lastMatch || that.fromStart) {
      switch (stamp) {
        case "W-":
          // Matches against a non-faction (Only wins 2 * power)
          that.debug("Filling sugar bar for a winner.");
          if (wID === that.chip) {
            that.sugarPoints[winner] += Math.ceil(10 * that.power); // Chip earns 10 for neutral wins
          } else {
            if (endgame) {
              that.sugarPoints[winner] += Math.ceil(that.power * 10);
            } else {
              that.sugarPoints[winner] +=
                wStress === "low" ? 1 : Math.ceil(that.power * 2); // Low stress winners only get 1
            }
          }

          didReduce = that.sugarReduction(winner);
          that.debug(JSON.stringify(that.sugarPoints));
          break;
        case "WL":
          if (winner === loser) {
            // Same faction match (only wins 2 * power)
            that.debug("Filling sugar bar for teammate battle.");
            that.sugarPoints[winner] += endgame
              ? Math.ceil(that.power * 10)
              : Math.ceil(that.power * 2);
            didReduce = that.sugarReduction(winner);
            didTeam = true;
          } else if (!stress && !endgame) {
            // Low-stress matches means only sugar is won
            // ==========================================
            that.debug("Filling sugar bars for low stress match.");

            // If Chip wins against someone...
            if (winner === "C") {
              that.sugarPoints[winner] += Math.ceil(10 * that.power);
            } else {
              // Anyone else refer to table
              that.sugarPoints[winner] +=
                wStress === "low"
                  ? 1 // Low stress winners only get 1
                  : Math.ceil(Math.ceil(lSugar[lr][wr] * 0.5) * that.power);
            }

            // If a non-Chip player loses against someone...
            if (loser !== "C") {
              that.sugarPoints[loser] +=
                wStress === "low"
                  ? Math.ceil(that.power * Math.ceil(lSugar[lr][wr] * 0.5))
                  : 0; // If winner stress is not low, i.e. loser is low stress, they gain nothing
            }

            didReduce = that.sugarReduction(winner);
            didReduce = that.sugarReduction(loser);
          } else {
            // Mid/High stress matches
            // =======================

            if (endgame) {
              // Med/High Rank battles full flip
              if (
                (wr === "M" && (wr === lr || lr === "H")) ||
                (wr === "H" && wr === lr)
              ) {
                that.sugarPoints[`${winner}${loser}`] += Math.ceil(
                  that.power * 100
                );
              }
              // Low rank battles give sugar
              else if (lr === "L" && wr !== lr) {
                that.sugarPoints[winner] += Math.ceil(that.power * 10);
              } else {
                that.sugarPoints[`${winner}${loser}`] += Math.ceil(
                  that.power * 50
                );
              }

              // Losers get 5
              that.sugarPoints[loser] += Math.ceil(that.power * 5);
            } else {
              // Against low rank = just sugar
              if (lr === "L") {
                that.debug("Filling sugar bar for a low rank battle.");
                that.sugarPoints[`${winner}`] +=
                  winner === "C"
                    ? Math.ceil(10 * that.power)
                    : Math.ceil(
                        that.power * Math.ceil(wSugar[wr][lr] * stress)
                      );
              } else {
                // Otherwise fill faction bar
                that.debug("Filling bar for a winner and loser.");

                // Chip is special again
                if (winner === "C") {
                  if (lr === "H")
                    that.sugarPoints[winner] += Math.ceil(50 * that.power);
                  if (lr === "M")
                    that.sugarPoints[winner] += Math.ceil(10 * that.power);
                } else {
                  // Normal, non-low rank people just use the table
                  if (wr !== "L") {
                    that.sugarPoints[`${winner}${loser}`] += Math.ceil(
                      that.power * Math.ceil(wSugar[wr][lr] * stress)
                    );
                  } else {
                    // Low rankers ONLY earn sugar
                    that.sugarPoints[winner] += Math.ceil(
                      that.power * Math.ceil(wSugar[wr][lr] * stress)
                    );
                  }
                }
              }

              if (loser !== "C") {
                that.sugarPoints[loser] += Math.ceil(
                  that.power * Math.ceil(lSugar[lr][wr] * stress)
                );
              }
            }
          }

          that.debug(JSON.stringify(that.sugarPoints));

          // Check for attacks when bars surpass 100 if not Chip
          if (winner !== "C") {
            matchup = that.sugarPoints[`${winner}${loser}`];
            if (matchup >= 100) {
              // Make attack and reduce points. 200/300 is probably only
              // possible in late game when power is multiplying gains
              if (matchup >= 300) {
                mapping.attack({ wL: data, that }, 3);
                that.sugarPoints[`${winner}${loser}`] -= 300;
              } else if (matchup >= 200) {
                mapping.attack({ wL: data, that }, 2);
                that.sugarPoints[`${winner}${loser}`] -= 200;
              } else if (matchup >= 100) {
                mapping.attack({ wL: data, that });
                that.sugarPoints[`${winner}${loser}`] -= 100;
              }

              // Set this guy too
              didAttack = true;
            }
          }

          break;
        case "-L":
          // Matches against a non-faction (only wins 1 * power)
          that.debug("Filling sugar bar for a loser.");

          // Chip and low stress losses earn NOTHING
          if (loser !== "C" && stress)
            that.sugarPoints[loser] += endgame
              ? Math.ceil(5 * that.power)
              : Math.ceil(1 * that.power);

          didReduce = that.sugarReduction(loser);
          that.debug(JSON.stringify(that.sugarPoints));
          break;
        default:
          break;
      }
    }

    if (!didAttack) {
      if (didReduce) {
        that.wlogPush.unshift(that.formatLog(data, "wasted sugar", 0));
      } else {
        let sugar = didTeam ? "gathered sugar" : "stockpiled sugar";
        that.wlogPush.unshift(that.formatLog(data, sugar, 0));
      }
    }
  },
  /**
   * Calculates the amount of sugar required to strike a certain region
   * @param {Object} data - Contains the payload of properties
   * @param {String} data.faction - The faction of the striker
   * @param {Array} data.regions - Array of IDs of neighbor regions to begin check
   * @param {Object[]} data.world - Contains the world we're checking against
   * @param {Number} distance - The amount of tiles we've had to traverse
   */
  calcSugar(data, distance = 0) {
    const { faction, regions, world } = data;
    let sugarCost = 100;
    const initial = faction.toLowerCase();
    // Store every neighbor's neighbor for the next possible iteration
    const moreregions = [];

    // Loop through ever neighbor
    for (let i = 0; i < regions.length; i += 1) {
      // Check neighbors of to-strike region for striker faction
      if (world[regions[i]].faction.startsWith(initial)) {
        sugarCost += distance * 25;
        return sugarCost;
      } else {
        moreregions.push(...world[regions[i]].neighbors);

        // Checked all the neighbors and nothing, loop through next set
        if (i === regions.length - 1) {
          return mapping.calcSugar(
            {
              faction,
              regions: moreregions,
              world
            },
            distance + 1
          );
        }
      }
    }
  },
  /**
   * The big chalupa. This is the function that starts upon receiving a new match to process.
   * @param {Object} that - Contains way more than it should, probably
   * @param {Object[]} that[].cachedWorld - The map and its current state
   * @param {Array} that[].wlog - ??
   * @param {Array} that[].wlogPush - ???
   * @param {Object} that[].rallyPoints - Contains the 4 rally points for each faction
   * @param {Function} that[].setPower - Sets the power
   * @param {String} that[].env - Current environment
   */
  updateMap(that) {
    const {
      cachedWorld,
      wlog,
      wlogPush,
      rallyPoints,
      formatLog,
      setPower,
      loopLogs,
      setCreated,
      createdWatch,
      fromStart,
      worldHistory,
      lastMatch,
      getRegion,
      setRegion,
      debug
    } = that;

    let wL = wlogPush.length < 100 ? wlog[wlogPush.length] : wlog[99];
    if (fromStart || !that.notHeroku) wL = worldHistory[wlogPush.length];

    // If we don't have a match to process, bail
    if (!wL) return false;

    // Adjust some variables per faction for pogs
    ["R", "B", "G", "Y"].forEach(t => {
      const regions = that[`${mapping.tColor(t)}Regions`];

      if (wL.rallies) {
        // Place the priority pog correctly
        if (regions.length > 0) {
          rallyPoints[t] = wL.rallies[`${t}Rally`];
        } else {
          rallyPoints[t] = false;
        }
      }
    });

    console.log(
      `${wL.w ? wL.w : "-"} vs ${wL.l ? wL.l : "-"} | ${wlogPush.length + 1}`
    );

    // Boost power if a main faction is dead
    let power = 1;
    if (that.redRegions.length === 0) power += 0.25;
    if (that.greenRegions.length === 0) power += 0.25;
    if (that.yellowRegions.length === 0) power += 0.25;
    if (that.blueRegions.length === 0) power += 0.25;
    setPower(power);

    // Get the winner/loser regions
    const winner = getRegion(wL.w);
    const loser = getRegion(wL.l);

    // Check for a sugar strike
    // ========================
    if (
      wL.strike >= 0 &&
      (!lastMatch || fromStart) &&
      that.sugarPoints[wL.w] >= 100 &&
      !winner.includes(wL.strike)
    ) {
      debug(`Strike on #${wL.strike}`);
      const struckReg = cachedWorld[wL.strike];

      if (struckReg.isCapital) {
        // Check if the capital was taken over
        if (
          (wL.strike === 4 && struckReg.faction !== "blue") ||
          (wL.strike === 21 && struckReg.faction !== "red") ||
          (wL.strike === 2 && struckReg.faction !== "yellow") ||
          (wL.strike === 23 && struckReg.faction !== "green") ||
          (wL.strike === 14 && struckReg.faction !== "chip")
        ) {
          setRegion(wL.strike, 1, mapping.tColor(wL.w));
        } else {
          // Capitals can only lose -1, but can't be flipped with strikes
          setRegion(wL.strike, struckReg.defense - 1);
        }
      } else if (!struckReg.isCapital) {
        setRegion(wL.strike, 1, mapping.tColor(wL.w));
      }

      // Deducts the striker's sugar
      that.sugarPoints[wL.w] -= wL.cost;

      debug(JSON.stringify(that.sugarPoints));

      wlogPush.unshift(
        formatLog(wL, "has fired the sugar", cachedWorld[wL.strike])
      );
      // ========================
      // Just a normal win
      // ========================
    } else if (winner && winner.length > 0) {
      debug(`We have a winner.`);

      // Check the 3 possible win conditions
      if (loser !== null) {
        // 1 — FACTION|n|FACTION = Stuff!
        mapping.fillBars(wL, "WL", that);
      } else {
        // 2 — FACTION|n|- = Just Sugar
        mapping.fillBars(wL, "W-", that);
      }
    } else if (loser && loser.length > 0) {
      debug(`We have a loser.`);
      // 3 — -|n|FACTION = Just Sugar
      mapping.fillBars(wL, "-L", that);
    } else if (wL.plague && (!lastMatch || fromStart)) {
      debug("Infecting the lands...");
      const pReg = cachedWorld[wL.region];
      const def =
        wL.plague === "HALF" ? Math.ceil(pReg.defense / 2) : wL.amount;
      setRegion(wL.region, def); // Plague region

      wlogPush.unshift(
        formatLog(wL, "has infected the lands", { ".key": false })
      );
    } else {
      debug(`They ded yo.`);

      // This faction is ded
      wlogPush.unshift(
        formatLog(wL, "has rolled in their grave", { ".key": false })
      );
    }
    // ========================
    // Finalizing some loop stuff
    // ========================

    if (
      (wlog.length > wlogPush.length && lastMatch) ||
      (fromStart && worldHistory.length > wlogPush.length)
    ) {
      console.log("Keep loopin", wlog.length, wlogPush.length);
      loopLogs();
      return false;
    } else if (!createdWatch) {
      setCreated();

      if (wlogPush[0] && wlogPush[0].fid === lastMatch) {
        console.log("We've finished looping initial matches. Going live.");
        that.lastMatch = false;
      }

      return true;
    } else {
      console.log("We've finished relooping all matches. Going live again.");
      that.lastMatch = false;
      that.fromStart = false;
    }

    return true;
  }
};

export default mapping;
