# Pocketworld

A metagame for the ranked multiplayer matches of Tooth and Tail.

This is the front-end facing side of the project. The backend shares the [map.js](https://gitlab.com/mastastealth/pocketworld/-/blob/master/src/map.js) as that is where all the primary game rule logic lives. The logic main lives here to allow the front-end to "replay" the whole season. 

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Lints and fixes files

```
yarn run lint
```

Use Node 10.
